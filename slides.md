
class: center, middle, title-slide
background-image: url(assets/infrastructure-avatar.png)

# GitLab Workflow

Winnie Hellmann  
[@winh](https://gitlab.com/winh)  
2019-03-14

---

class: center

# DevOps cycle

![devops cycle diagram](assets/devops-loop-and-spans-small.png)

(https://about.gitlab.com/stages-devops-lifecycle/)

---

# Plan

**irgendwann erstellt irgendwer einen Issue:**

![screenshot of a bug report](assets/bug-report.png)  
(https://about.gitlab.com/handbook/communication/#everything-starts-with-an-issue)

---

# Plan

### optional, aber hilfreich

**irgendwer reproduziert den Bug auf gitlab.com:**

![screenshot of a comment after reproducing a bug](assets/reproduced-comment.png)

![screenshot of adding a reproduced label to an issue](assets/reproduced-label.png)

**irgendwer benachrichtigt Product Manager und Engineering Manager:**

![screenshot of a comment that pings managers](assets/ping-managers.png)

---

# Plan

**ein paar Labels werden über den Issue geschüttet:**

![screenshot of adding labels to an issue](assets/issue-labels.png)

![screenshot of adding severity and priority to an issue](assets/severity-priority-labels.png)


**und der Product Manager weist einen Meilenstein zu:**

![screenshot of adding a milestone to an issue](assets/product-manager-milestone.png)

![screenshot of adding an issue to the backlog](assets/product-manager-backlog.png)

---

# Plan

**manchmal weist der Engineering Manager den Meilenstein zu:**

![screenshot of a comment for scheduling an issue](assets/engineering-manager-scheduling.png)

![screenshot of adding a milestone to an issue](assets/engineering-manager-milestone.png)

**und manchmal wird auch etwas nachgeholfen:**

![screenshot of adding a milestone to an issue](assets/manager-of-one-milestone.png)

(https://about.gitlab.com/handbook/values/#managers-of-one)

---

# Plan

**automagisch landet es damit im Issue Board:**

![screenshot of issues in an issue board](assets/issue-board.png)

---

# Plan

**issue wird zugewiesen:**

![screenshot of assigning an issue](assets/issue-assigned.png)

**und die Arbeit beginnt:**

![screenshot of adding "in dev" label to an issue](assets/issue-in-dev.png)

![screenshot of assigning an issue](assets/assigned-issues-list.png)

---

# Create

**coden, coden, coden...**

![animation of a cat typing on a laptop](assets/typing-cat.gif)

---

# Create

**`git add --all`, `git commit`, `git push`:**

![screenshot of git push to gitlab.com](assets/git-push.png)

---

# Create

**ein neuer Merge Request – sogar noch warm:**

![screenshot of new merge request](assets/open-merge-request.png)

---

# Create

**nur wenige `git push`es später:**

![screenshot of git pushes to merge request](assets/more-pushes.png)

---

# Create

**wenn fertig, wird gereviewt:**

![screenshot of comment to reviewer](assets/reviewer-comment.png)

![screenshot of assigning to reviewer](assets/reviewer-assign.png)

**manchmal weniger...**

![screenshot of reviewer approval](assets/reviewer-lgtm.png)

---

# Create

**...manchmal auch ein bisschen mehr:**

![screenshot of many review comments](assets/many-comments.gif)

---

# Create

**außerdem gibt es noch ungefähr drölfzig automatisierte Checks:**

![screenshot of merge request widget](assets/merge-request-widget.png)

---

# Verify

**zum Beispiel die Pipeline:**

![screenshot of a GitLab CI pipeline](assets/pipeline.png)

---

# Das war's... erstmal?

### weiter geht es nach der nächsten Maus

- Verify: GitLab CI (oder was ist eigentlich diese Pipeline)
- Release: GitLab Pages (oder wie man mit voll dynamisch Homepages baut)
- Wie funktioniert so ein GitLab Release?

---

class: warning, center, middle

# Warnung

Die Personen und die Handlung in diesem Vortrag sind nicht erfunden.

Etwaige Ähnlichkeiten mit tatsächlichen Begebenheiten oder lebenden oder verstorbenen Personen sind nicht rein zufällig.
